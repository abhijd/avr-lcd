# LCD interfacing with AVR Atmega32

Voltage read with the build-in ADC of the Atmega 32 and the result is displayed on the LCD in real time. The ADC measures voltage across a simple variable voltage source. The variable voltage source represents a variable sensors values in a real environment like IR sensor or LM56 thermostat. 


## Simulating

To simulate, you must install all the software listed in the prerequisite section and then following the "How to simulate" section 


### Prerequisites

 * Proteus Design Suite
 

### How-to-simulate
 
 * Clone the repo in your local directory
 * Start Proteus Design Suite
 * In Proteus, open either hw3.DSN 
 * When the design opens, double-click on the Atmega32 microcontroller that opens the "edit component" pop-up
 * From the edit component, add the location of the hw3.hex file in the program file field and click OK.
 * Press the play button from the bottom left menu. Voila!!
 * Once started, you can drag the rheostat to change the voltage across the ADC port.

 
## Changing the program code

  * To change the program code, you may use AVR Studio/WinAVR to open the hw3.c file and change as desired.
  * Once changed, compile into hex file again using AVR Studio/ WinAVR, and load it again to the Atmega32 following the steps from "How-to-simulate"
 
  
## Hardware Implementation 

  * A simple voltage regulator circuit should be used to power the microcontroller
  * Before printing the circuit in board, experimentally make sure the hardware implementation work because the real world is not always similar to simulation world. We had to do several tweaks to make it work in the real world.  
  * The main.hex file must me burned (not literally) in the Atmega32 microcontroller using AVR programmer or some universal programmer that supports it. 
 
 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 

