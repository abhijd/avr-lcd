
#define F_CPU 8000000UL
#include<avr/io.h>
#include<util/delay.h>



#define LP PORTB
#define DDRLP DDRB

#define LCNT PORTD
#define DDR_LCNT DDRD
#define RS PORTD7
#define E PORTD6

void SEND_CMD(unsigned char);
void LCD_Init(void);
void SEND_TXT(unsigned char);
void display_adc(unsigned char);

void init_adc();
unsigned char read_adc();

int main(void)
{	
	DDRLP=0xFF;
	DDR_LCNT=0xFF;
	DDRC=0xFF;
	
	LCD_Init();

	SEND_CMD(0X84);

	_delay_ms(200);
		
	SEND_TXT('A');   
	SEND_TXT('D');      
	SEND_TXT('C');    
	SEND_TXT(':');    
	SEND_TXT(' ');
	
	SEND_TXT(' ');
	SEND_TXT(' ');
	SEND_TXT(' ');
	SEND_TXT(' ');
	SEND_TXT(' ');
	SEND_TXT('V');
	SEND_CMD(0X10);

	init_adc();


	
	unsigned char val;
	while(1)
	{
		SEND_CMD(0X89);
		val=read_adc();		
		PORTC=val;
		display_adc(val);
		PORTC=val;		
		_delay_ms(200);
	}
	return 1;
}

void init_adc()
{
	ADMUX=0;
	ADMUX|=(1<<REFS1)|(1<<REFS0);
	ADMUX|=(1<<ADLAR);
	ADCSRA=(1<<ADPS2)|(1<<ADPS0);
	ADCSRA=(1<<ADEN);
}

unsigned char read_adc(void)
{
	//int I;
	ADCSRA|=_BV(ADSC);
	while(!(ADCSRA & 0x10));
	//I=ADCH;
	return ADCH;
	ADCSRA&=~(1<<ADSC);

}

void SEND_CMD(unsigned char cmd)
{ 
	LP=cmd;
	
	LCNT&=~(1<<RS);//rs=0;
	LCNT|=(1<<E);//en=1;
	LCNT&=~(1<<E);//en=0;
	_delay_ms(20);
}


void LCD_Init(void)
{
	SEND_CMD(0x38);
	SEND_CMD(0x38);
	SEND_CMD(0x38); 
	SEND_CMD(0x0C);  
	SEND_CMD(0x06);  
	SEND_CMD(0x80); 
}	

void SEND_TXT(unsigned char txt)
{
	LP=txt;
	LCNT|=(1<<RS);//rs=1;
	LCNT|=(1<<E);//en=1;
	LCNT&=~(1<<E);//en=0;
	_delay_ms(20);
}

void display_adc(unsigned char value)
{
	unsigned char id51,id53,id54;
	unsigned char d1,d2,d3,d4;
	float test;
	
	test=(value*2.56)/255;
	
	id51=0;
	while(test>1){
		id51=id51+1;
		test=test-1;
	}
    
	id53=0;
	while(test>.1){
		id53=id53+1;
		test=test-.1;
	}
	
	id54=0;
	while(test>.01){
		id54=1+id54;
		test=test-.01;
	} 
	
	d1=48+id51;
	d2=0x2E;
	d3=48+id53;
	d4=48+id54;
	
	SEND_TXT(d1);
	SEND_TXT(d2);
	SEND_TXT(d3);
	SEND_TXT(d4);
}
